package com.dengun.foxdarkmaster.tdd

import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.Assert
import org.junit.Test


@RunWith(Suite::class)
@Suite.SuiteClasses(
    TestFeatureLogin::class,
    TestFeatureLogout::class,
    TestFeatureNavigate::class,
    TestFeatureUpdate::class
)

class FeatureTestSuite {
    // the class remains empty,
    // used only as a holder for the above annotations
}

class TestFeatureLogin {
    @Test
    fun testAssertEquals() {
        Assert.assertEquals("failure - strings are not equal", "text", "text")
    }
}

class TestFeatureLogout {
    @Test
    fun testAssertEquals() {
        Assert.assertEquals("failure - strings are not equal", "text", "text")
    }
}

class TestFeatureNavigate {
    @Test
    fun testAssertEquals() {
        Assert.assertEquals("failure - strings are not equal", "text", "text")
    }
}

class TestFeatureUpdate {
    @Test
    fun testAssertEquals() {
        Assert.assertEquals("failure - strings are not equal", "text", "text")
    }
}
