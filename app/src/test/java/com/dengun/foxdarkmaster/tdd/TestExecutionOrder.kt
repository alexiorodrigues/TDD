package com.dengun.foxdarkmaster.tdd

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class TestExecutionOrder {
    @Test
    fun testA() {
        println("first")
    }

    @Test
    fun testB() {
        println("second")
    }

    @Test
    fun testC() {
        println("third")
    }
}