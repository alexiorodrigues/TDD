package com.dengun.foxdarkmaster.tdd

import org.junit.Test
import org.junit.Assert.assertThat
import org.junit.Assert.fail
import org.hamcrest.CoreMatchers.`is`


class ExceptionTesting {
    @Test
    fun testExceptionMessage() {
        try {
            ArrayList<Any>()[0]
            fail("Expected an IndexOutOfBoundsException to be thrown")
        } catch (anIndexOutOfBoundsException: IndexOutOfBoundsException) {
            assertThat(anIndexOutOfBoundsException.message, `is`("Index: 0, Size: 0"))
        }

    }
}