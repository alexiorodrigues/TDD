package com.dengun.foxdarkmaster.tdd

import org.junit.Assert
import org.junit.Test

class TimeoutTests {
    // Timeout in milliseconds
    @Test(timeout=20)
    fun testAssertEquals() {
        Assert.assertEquals("failure - strings are not equal", "text", "text")
    }
}