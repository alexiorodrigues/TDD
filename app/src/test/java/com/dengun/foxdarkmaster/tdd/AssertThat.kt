package com.dengun.foxdarkmaster.tdd

import org.junit.Test

import org.junit.Assert.assertThat
import org.hamcrest.CoreMatchers.*


// Instead of doing this
// assertTrue(responseString.contains("color") || responseString.contains("colour"));

// Do This
// assertThat(responseString, anyOf(containsString("color"), containsString("colour")));

class AssertThat {
    @Test
    fun testAssertThat1() {
        assertThat(3, `is`(3))
    }

    @Test
    fun testAssertThat2() {
        assertThat(3, `is`(not(4)))
    }

    @Test
    fun testAssertThat3() {
        var responseString = "color:#333"
        assertThat(responseString, either(containsString("color")).or(containsString("colour")))
    }

    @Test
    fun testAssertThat4() {
        var myList = listOf("3")
        assertThat(myList, hasItem("3"))
    }
}