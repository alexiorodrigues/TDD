package com.dengun.foxdarkmaster.tdd

import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.anyOf
import org.hamcrest.CoreMatchers.both
import org.hamcrest.CoreMatchers.containsString
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.everyItem
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.sameInstance
import org.hamcrest.CoreMatchers.startsWith
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNotSame
import org.junit.Assert.assertNull
import org.junit.Assert.assertSame
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue

import java.util.Arrays

import org.hamcrest.core.CombinableMatcher
import org.junit.Test


class Assertions {
    @Test
    fun testAssertArrayEquals() {
        val expected = "trial".toByteArray()
        val actual = "trial".toByteArray()
        assertArrayEquals("failure - byte arrays not same", expected, actual)
    }

    @Test
    fun testAssertEquals() {
        assertEquals("failure - strings are not equal", "text", "text")
    }

    @Test
    fun testAssertFalse() {
        assertFalse("failure - should be false", false)
    }

    @Test
    fun testAssertNotNull() {
        assertNotNull("should not be null", Any())
    }

    @Test
    fun testAssertNotSame() {
        assertNotSame("should not be same Object", Any(), Any())
    }

    @Test
    fun testAssertNull() {
        assertNull("should be null", null)
    }

    @Test
    fun testAssertSame() {
        val aNumber = Integer.valueOf(768)
        assertSame("should be same", aNumber, aNumber)
    }

    // JUnit Matchers assertThat
    @Test
    fun testAssertThatBothContainsString() {
        assertThat("albumen", both(containsString("a")).and(containsString("b")))
    }

    @Test
    fun testAssertThatHasItems() {
        assertThat(Arrays.asList("one", "two", "three"), hasItems("one", "three"))
    }

    @Test
    fun testAssertThatEveryItemContainsString() {
        assertThat(Arrays.asList(*arrayOf("fun", "ban", "net")), everyItem(containsString("n")))
    }

    // Core Hamcrest Matchers with assertThat
    @Test
    fun testAssertThatHamcrestCoreMatchers() {
        assertThat("good", allOf(equalTo("good"), startsWith("good")))
        assertThat("good", not(allOf(equalTo("bad"), equalTo("good"))))
        assertThat("good", anyOf(equalTo("bad"), equalTo("good")))
        assertThat(7, not(CombinableMatcher.either(equalTo(3)).or(equalTo(4))))
        assertThat(Any(), not(sameInstance(Any())))
    }

    @Test
    fun testAssertTrue() {
        assertTrue("failure - should be true", true)
    }
}
